package br.edu.up.prova4bim;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

  BancoDeDados banco;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    banco = new BancoDeDados(this);
  }

  public void onClickBaixar(View v){

    String url = "http://cadastro-de-produtos.appspot.com/ws/produtos";
    RequestQueue queue = Volley.newRequestQueue(this);

    StringRequest request = new StringRequest(url,
        new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {
            Log.d("PROVA", response);
            try {
              JSONArray jsonArray = new JSONArray(response);
              for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String id = jsonObject.getString("id");
                Produto produto = banco.buscar(id);
                if (produto == null){
                  String nome = jsonObject.getString("nome");
                  String valor = jsonObject.getString("valor");
                  produto = new Produto(id, nome, valor);
                  banco.gravar(produto);
                }
              }

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        }, new Response.ErrorListener() {
      @Override
      public void onErrorResponse(VolleyError error) {
        Log.d("PROVA", "Falha na requisição...");
      }
    });

    queue.add(request);
    queue.start();
  }

  public void onClickListar(View v){
    Intent intent = new Intent(this, ListaActivity.class);
    startActivityForResult(intent,0);
  }
}