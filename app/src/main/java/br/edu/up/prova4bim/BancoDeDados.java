package br.edu.up.prova4bim;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Java que representa o banco de dados.
 */
public class BancoDeDados extends SQLiteOpenHelper {


    public BancoDeDados(Context context) {
        super(context, "exemplo", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE `produtos` (`id` INTEGER PRIMARY KEY AUTOINCREMENT,`idGoogle` TEXT, `nome` TEXT, `valor` TEXT )";
        db.execSQL(sql);
    }

    public void gravar(Produto produto){

        ContentValues valores = new ContentValues();
        valores.put("nome", produto.getNome());
        valores.put("idGoogle", produto.getIdGoogle());
        valores.put("valor", produto.getValor());

        //Carrega o banco SQLite;
        SQLiteDatabase db = getWritableDatabase();

        if (produto.getId() > 0){
            String id = String.valueOf(produto.getId());
            db.update("produtos", valores, "id=?", new String[]{id});
        } else {
            db.insert("produtos", null, valores);
        }
        db.close();
    }

    public void excluir(Produto produto){

        String id = String.valueOf(produto.getId());
        SQLiteDatabase db = getWritableDatabase();
        db.delete("produtos", "id=?", new String[]{id});
        db.close();

    }

    public List<Produto> listar(){

        List<Produto> lista = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("produtos", null, null, null, null,null, null);

        while (cursor.moveToNext()){

            int idx0 = cursor.getColumnIndex("id");
            int idx1 = cursor.getColumnIndex("idGoogle");
            int idx2 = cursor.getColumnIndex("nome");
            int idx3 = cursor.getColumnIndex("valor");

            Produto produto = new Produto();
            produto.setId(cursor.getInt(idx0));
            produto.setIdGoogle(cursor.getString(idx1));
            produto.setNome(cursor.getString(idx2));
            produto.setValor(cursor.getString(idx3));

            lista.add(produto);
        }
        db.close();
        return lista;
    }

    public Produto buscar(int id){

        Produto produto = null;

        String str = String.valueOf(id);
        SQLiteDatabase db = getReadableDatabase();
        //select * from pessoas where id = ?;
        Cursor cursor = db.query("produtos",null, "id=?", new String[]{str}, null,null,null);

        if (cursor.moveToNext()){

          int idx0 = cursor.getColumnIndex("id");
          int idx1 = cursor.getColumnIndex("idGoogle");
          int idx2 = cursor.getColumnIndex("nome");
          int idx3 = cursor.getColumnIndex("valor");

          produto = new Produto();
          produto.setId(cursor.getInt(idx0));
          produto.setIdGoogle(cursor.getString(idx1));
          produto.setNome(cursor.getString(idx2));
          produto.setValor(cursor.getString(idx3));
        }
        db.close();
        return produto;
    }

    public Produto buscar(String idGoogle){

    Produto produto = null;

    SQLiteDatabase db = getReadableDatabase();
    //select * from pessoas where idGoogle = ?;
    Cursor cursor = db.query("produtos",null, "idGoogle=?", new String[]{idGoogle}, null,null,null);

    if (cursor.moveToNext()){

      int idx0 = cursor.getColumnIndex("id");
      int idx1 = cursor.getColumnIndex("idGoogle");
      int idx2 = cursor.getColumnIndex("nome");
      int idx3 = cursor.getColumnIndex("valor");

      produto = new Produto();
      produto.setId(cursor.getInt(idx0));
      produto.setIdGoogle(cursor.getString(idx1));
      produto.setNome(cursor.getString(idx2));
      produto.setValor(cursor.getString(idx3));
    }
    db.close();
    return produto;
  }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       //Necessário quando for atualizar o banco.
    }
}
