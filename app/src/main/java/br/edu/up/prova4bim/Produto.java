package br.edu.up.prova4bim;


import java.io.Serializable;

public class Produto implements Serializable {

  private int id;
  private String idGoogle;
  private String nome;
  private String valor;

  public Produto(){
  }

  public Produto(String idGoogle, String nome, String valor) {
    this.idGoogle = idGoogle;
    this.nome = nome;
    this.valor = valor;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getIdGoogle() {
    return idGoogle;
  }

  public void setIdGoogle(String idGoogle) {
    this.idGoogle = idGoogle;
  }

  public String getValor() {
    return valor;
  }

  public void setValor(String valor) {
    this.valor = valor;
  }

  @Override
  public String toString() {
    return nome + ", " + valor;
  }
}