package br.edu.up.prova4bim;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListaActivity extends AppCompatActivity {

  ListView listView;
  ArrayAdapter<Produto> adapter;
  List<Produto> lista;
  BancoDeDados banco;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista);

    listView = (ListView) findViewById(R.id.listView);
    banco = new BancoDeDados(this);
    lista = banco.listar();

    int layout = android.R.layout.simple_list_item_1;
    adapter = new ArrayAdapter<>(this, layout, lista);
    listView.setAdapter(adapter);

    listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
      @Override
      public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        final Produto produto = adapter.getItem(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(ListaActivity.this);
        builder.setTitle("ATENÇÃO");
        builder.setMessage("Tem certeza que seja excluir?");
        builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            banco.excluir(produto);
            lista.clear();
            lista.addAll(banco.listar());
            adapter.notifyDataSetChanged();
          }
        });

        builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            //não faz nada...
          }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        return true;
      }
    });
  }
}